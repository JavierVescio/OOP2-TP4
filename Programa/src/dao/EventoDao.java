package dao;

import java.util.GregorianCalendar;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import datos.Cliente;
import datos.Evento;

public class EventoDao {
	private static Session session;
	private Transaction tx;

	private void iniciaOperacion() throws HibernateException {
		session = HibernateUtil.getSessionFactory().openSession();
		tx = session.beginTransaction();
	}

	private void manejaExcepcion(HibernateException he) throws HibernateException {
		tx.rollback();
		throw new HibernateException("ERROR en la capa de acceso a datos", he);
	}
	
	public int agregarEvento(Evento objeto) throws HibernateException  {
		int id=0;
		try {
			iniciaOperacion();
			id = Integer.parseInt(session.save(objeto).toString());
			tx.commit();
		}catch(HibernateException he) {
			manejaExcepcion(he);
			throw he;
		}finally {
			session.close();
		}
		return id;
	}
	
	public void actualizar(Evento objeto) throws HibernateException {
		try {
			iniciaOperacion();
			session.update(objeto);
			tx.commit();
		}catch(HibernateException he) {
			manejaExcepcion(he);
			throw he;
		}finally {
			session.close();
		}
	}
	
	public Evento traerEvento( long idEvento) throws HibernateException {
		Evento objeto = null ;
		try {
			iniciaOperacion();
			objeto = (Evento)session.get(Evento.class, idEvento);
		} finally {
			session.close();
		}
		return objeto;
	}
	
	public Evento traerEventoConSusClientes(long idEvento) throws HibernateException {
		Evento objeto = null;
		try {
			iniciaOperacion();
			objeto = (Evento)session.get(Evento.class, idEvento);
			Hibernate.initialize(objeto.getClientes());
		}finally {
			session.close();
		}
		return objeto;
	}
}
