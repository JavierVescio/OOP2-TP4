package test;

import datos.Cliente;
import datos.Evento;
import negocio.ClienteABM;
import negocio.EventoABM;

public class TestAgregarClienteAEvento {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClienteABM clienteAbm = new ClienteABM();
		EventoABM eventoAbm = new EventoABM();
		try {
			long idCliente = 1;
			long idEvento = 2;
			
			Cliente cliente = clienteAbm.traerCliente(idCliente);
			Evento evento = eventoAbm.traerEvento(idEvento);
			
			eventoAbm.agregarClienteAEvento(cliente, evento);
		
		}catch(Exception e){
			System.out.println("TestAgregarClienteAEvento: "+e.getMessage());
		}
	
	}
}
