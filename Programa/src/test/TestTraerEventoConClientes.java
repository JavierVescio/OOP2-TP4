package test;

import datos.Cliente;
import datos.Evento;
import negocio.EventoABM;

public class TestTraerEventoConClientes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventoABM eventoabm = new EventoABM();
		
		long idEvento = 2;
		try {
			Evento evento = eventoabm.traerEventoConSusClientes(idEvento);
			
			/*Evento evento = eventoabm.traerEvento(idEvento);
			 * Eso tambien funciona. Es decir, trae el evento y los clientes. Re flash! 
			 * */
			
			System.out.println(evento);
			
			for(Cliente cliente:evento.getClientes()){
				System.out.println(cliente);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
	}

}
