package test;

import datos.Cliente;
import datos.Evento;
import negocio.ClienteABM;
import negocio.EventoABM;

public class TestEliminarClienteDeEvento {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventoABM eventoABM = new EventoABM();
		ClienteABM clienteABM = new ClienteABM();

		long idCliente=1;
		long idEvento=2;

		try {
			Cliente cliente = clienteABM.traerCliente(idCliente);
			Evento evento = eventoABM.traerEventoConSusClientes(idEvento);
			
			eventoABM.eliminarClienteDeEvento(cliente, evento);
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
