package test;

import negocio.ClienteABM;
import negocio.EventoABM;
import datos.Cliente;
import datos.Evento;

public class TestAgregarEventoAlCliente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventoABM eventoAbm = new EventoABM();
		ClienteABM clienteAbm = new ClienteABM();
		try {
			long idEvento = 2;
			long idCliente = 1;
			
			Evento evento = eventoAbm.traerEvento(idEvento);
			Cliente cliente = clienteAbm.traerCliente(idCliente);
			
			clienteAbm.agregarEventoACliente(evento, cliente);
			
		}catch(Exception e){
			System.out.println("TestAgregarEventoAlCliente: "+e.getMessage());
		}
	}

}
