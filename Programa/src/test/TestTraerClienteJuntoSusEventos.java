package test;

import datos.Cliente;
import datos.Evento;
import negocio.ClienteABM;

public class TestTraerClienteJuntoSusEventos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClienteABM clienteABM = new ClienteABM();
		
		long idCliente = 2;
		
		try {
			Cliente cliente = clienteABM.traerClienteYEventos(idCliente);
			
			System.out.println("\n"+cliente);
			for(Evento evento: cliente.getEventos()){
				System.out.println("\n"+evento);
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}

	}

}
