package test;

import negocio.ClienteABM;
import negocio.EventoABM;
import datos.Cliente;
import datos.Evento;

public class TestEliminarEventoDeCliente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClienteABM clienteABM = new ClienteABM();
		EventoABM eventoABM = new EventoABM();

		long idEvento=2;
		long idCliente=1;

		try {
			Evento evento = eventoABM.traerEvento(idEvento);
			Cliente cliente = clienteABM.traerClienteYEventos(idCliente);
			
			clienteABM.eliminarEventoDeCliente(evento, cliente);
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
