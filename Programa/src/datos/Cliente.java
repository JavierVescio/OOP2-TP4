package datos;

import java.util.GregorianCalendar;
import java.util.HashSet;

import negocio.Funciones;

import java.util.Set;


public class Cliente {
	private long idCliente;
	private String apellido;
	private String nombre;
	private String dni;
	private GregorianCalendar fechaDeNacimiento;
	private boolean baja;
	private Set<Evento> eventos = null;

	public Cliente(){} //siempre hay que implementar el contructor vacio

	public Cliente(String apellido, String nombre, String dni, GregorianCalendar fechaDeNacimiento) {
		super();
		this.apellido = apellido;
		this.nombre = nombre;
		this.dni = dni;
		this.fechaDeNacimiento = fechaDeNacimiento;
		this.baja = false ;
	}

	public long getIdCliente() {
		return idCliente;
	}

	protected void setIdCliente(long idCliente) {
		//Siempre va protected, para que no sea modificado
		this.idCliente = idCliente;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public GregorianCalendar getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}

	public void setFechaDeNacimiento(GregorianCalendar fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

	public boolean isBaja() {
		return baja;
	}

	public void setBaja(boolean baja) {
		this.baja = baja;
	}

	public Set<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(Set<Evento> eventos) {
		this.eventos = eventos;
	}

	public void setPrestamos(Set<Evento> eventos) {
		this.eventos = eventos;
	}

	public boolean agregar(Evento evento){
		if (eventos == null){
			eventos = new HashSet<Evento>();
		}
		return eventos.add(evento);
	}

	public boolean eliminar(Evento evento){
		boolean salida = false;
		if (eventos != null){
			salida = eventos.remove(evento);
		}
		return salida;
	}

	@Override
	public String toString(){
		return (idCliente+" "+apellido+" "+nombre+" DNI: "+dni+ " Fecha de Nacimiento: "+Funciones.traerFechaCorta(fechaDeNacimiento)+" "+baja);
	}
	
	public boolean equals(Cliente cliente){
		return (idCliente == cliente.getIdCliente());
	}
	
	@Override
	public int hashCode() {
		return (int) idCliente;
	}
	
    @Override
    public boolean equals(Object o) {
    	boolean salida;
    	
        if (o == this) 
        	salida = true;
        else if (!(o instanceof Cliente)) {
        	salida = false;
        }
        else {
        	Cliente cliente = (Cliente)o;
        	salida = cliente.idCliente == idCliente;
        }
        return salida;
    }
    
    /*Cuando se llama a contains del Set, se hace o.equals(e), donde o es el Cliente
     * y e es uno de los tantos elementos de tipo Object que contiene el Set. 
     * Al ser un Object, no hay forma que entre al equals(Cliente cliente) de esta clase.
     * Por eso fue necesario definir el equals(Object o) que recibe como parametro un object, que 
     * es la forma en que se almacenan los elementos en el Set.
     * Como nosotros sabemos que el Object es un Cliente, lo casteamos a Cliente despu�s.
     * La funcion Contains y Remove, hacen uso de equals() e inmediatamente despu�s de hashCode().
     * HashCode() devuelve un n�mero que identifica a un objeto. Si no se redefine tambi�n a hashCode(),
     * la funcion Contains o Remove nunca encontraran en el Set al Cliente que pasamos como parametro.
     * 
     * Quizas uno podria preguntarse despues que tan correcto es hacer que el hashCode retorne solamente el idCliente,
     * ya que nunca antes habiamos redefinido esta funcion; aunque teniendo en cuenta que es algo irrepetible el id, 
     * en principio me parece que estaria perfecto lo hecho aqui. Ademas, como respaldo adicional,
     * fue probado y funciona el programa, precisamente el TestEliminarClienteDeEvento, que hace uso de las implementaciones
     * de hashCode() y equals(object) al hacer un clientes.remove(cliente).
     * */
}
