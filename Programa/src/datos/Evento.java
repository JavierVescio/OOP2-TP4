package datos;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.plaf.synth.SynthSeparatorUI;

import negocio.Funciones;

public class Evento {
	private long idEvento;
	private String evento;
	private GregorianCalendar fecha;
	private Set<Cliente> clientes=null;

	public Evento(){}

	public Evento(String evento, GregorianCalendar fecha) {
		super();
		this.evento = evento;
		this.fecha = fecha;
	}

	public long getIdEvento() {
		return idEvento;
	}

	protected void setIdEvento(long idEvento){
		this.idEvento = idEvento;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public GregorianCalendar getFecha() {
		return fecha;
	}

	public void setFecha(GregorianCalendar fecha) {
		this.fecha = fecha;
	}

	public Set<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(Set<Cliente> clientes) {
		this.clientes = clientes;
	}

	public boolean agregar(Cliente cliente){
		if (clientes == null) {
			clientes = new HashSet<Cliente>();
		}
		return clientes.add(cliente);
	}

	public boolean eliminar(Cliente cliente){
		boolean salida = false;
		if (clientes != null) {
			salida = clientes.remove(cliente);
		}
		return salida;
	}

	public String toString() {
		return idEvento+" "+evento+" "+Funciones.traerFechaCorta(fecha);
	}
	
	public boolean equals(Evento evento){
		return (idEvento == evento.getIdEvento());
	}
	
    @Override
    public boolean equals(Object o) {
    	boolean salida;
    	
        if (o == this) 
        	salida = true;
        else if (!(o instanceof Evento)) {
        	salida = false;
        }
        else {
        	Evento evento = (Evento)o;
        	salida = evento.idEvento == idEvento;
        }
        return salida;
    }
    
    @Override
	public int hashCode() {
    	return (int)idEvento;
    }
    
}
