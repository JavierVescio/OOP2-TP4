package negocio;

import java.util.GregorianCalendar;

import dao.EventoDao;
import datos.Cliente;
import datos.Evento;

public class EventoABM {
	EventoDao dao = new EventoDao();
	
	public void agregarClienteAEvento(Cliente cliente, Evento evento) throws Exception {
		if (cliente != null) {
			if (evento.agregar(cliente)) {
				dao.actualizar(evento);
			}
			else{
				throw new Exception("No se pudo agregar el cliente al evento");
			}
		}
		else {
			throw new Exception("El cliente es nulo");
		}
	}
	
	public void eliminarClienteDeEvento(Cliente cliente, Evento evento) throws Exception {
		if (cliente != null) {
			if (evento.eliminar(cliente)) {
				dao.actualizar(evento);
			}
			else {
				throw new Exception("No se pudo eliminar el cliente");
			}
		}
		else {
			throw new Exception("El cliente es nulo");
		}
	}
	
	public int agregarEvento(String evento, GregorianCalendar fecha){
		Evento objEvento = new Evento(evento, fecha);
		return dao.agregarEvento(objEvento);
	}
	
	public Evento traerEvento(long idEvento) throws Exception {
		Evento evento = dao.traerEvento(idEvento);
		if (evento == null){
			throw new Exception("El evento no existe");
		}
		return evento;
	}
	
	public Evento traerEventoConSusClientes(long idEvento) throws Exception {
		Evento evento = dao.traerEventoConSusClientes(idEvento);
		if (evento == null) {
			throw new Exception("El evento no existe");
		}
		return evento;
	}
}
