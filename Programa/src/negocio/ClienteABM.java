package negocio;

import java.util.GregorianCalendar;
import java.util.List;

import dao.ClienteDao;
import datos.Cliente;
import datos.Evento;

public class ClienteABM {
	ClienteDao dao = new ClienteDao();

	public Cliente traerCliente(long idCliente) throws Exception{
		Cliente c= dao.traerCliente(idCliente);
		if (c==null){
			throw new Exception("No existe el cliente");
		}
		return c;
	}

	public Cliente traerCliente(String dni) throws Exception {
		Cliente c = dao.traerCliente(dni);
		if (c==null){
			throw new Exception("No existe el cliente");
		}
		return c;
	}

	public int agregar(String apellido, String nombre, String dni, GregorianCalendar fechaDeNacimiento) throws Exception {
		Cliente c = dao.traerCliente(dni);
		if (c == null)
			c = new Cliente(apellido, nombre, dni,fechaDeNacimiento);
		else
			throw new Exception("El cliente con el dni ingresado ya existe!");
		return dao.agregar(c);
	}

	public void modificar(Cliente c) throws Exception{
		dao.actualizar(c);
	}

	public void eliminar(long idCliente) throws Exception { /*en este caso es f�sica en gral. no se se
		aplicar�a este caso de uso, si se hiciera habr�a que validar que el cliente no tenga
		dependencias*/
		Cliente c = dao.traerCliente(idCliente);
		if (c==null){
			throw new Exception("No hay cliente para eliminar");
		}
		else
			dao.eliminar(c);
	}
	
	public Cliente traerClienteYEventos(long idCliente) throws Exception{
		Cliente c= dao.traerClienteYEventos(idCliente);
		if (c==null){
			throw new Exception("No existe el cliente");
		}
		return c;
	}
	
	public void agregarEventoACliente(Evento evento, Cliente cliente) throws Exception {
		if (evento != null) {
			if (cliente.agregar(evento)) {
				dao.actualizar(cliente);
			}
			else{
				throw new Exception("No se pudo agregar el evento al cliente");
			}
		}
		else {
			throw new Exception("El evento es nulo");
		}
	}
	
	public void eliminarEventoDeCliente(Evento evento, Cliente cliente) throws Exception {
		if (evento != null) {
			if (cliente.eliminar(evento)) {
				dao.actualizar(cliente);
			}
			else {
				throw new Exception("No se pudo eliminar el evento");
			}
		}
		else {
			throw new Exception("El evento es nulo");
		}
	}
}
